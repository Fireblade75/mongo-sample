const express = require('express')
const { MongoClient } = require('mongodb')
const bodyParser = require('body-parser')
const path = require('path')
const apiRouter = require('./apiRouter')
require('dotenv').config()

const port = process.env.PORT || 3000
const app = express()

const mClient = new MongoClient(process.env.MONGO_URL, { useNewUrlParser: true })
const dbName = 'mongo-sample';

app.use(bodyParser.urlencoded({ extended: false }))
app.use(express.static(path.join(__dirname, '../static/')))

app.use((req, res, next) => {
    req.db = mClient.db(dbName)
    next()
})

app.use('/api', apiRouter)

mClient.connect().then(() => {
    console.log('Connected to mongo db')
    app.listen(port, () => {
        console.log('Listening on port ' + port)
    })
}).catch(() => {
    console.error('Cannot connect to the mongo db')
    console.error('Closing server')
})

