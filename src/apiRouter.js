const { Router } = require('express')

const router = Router()

router.post('/users', (req, res) => {
    const {firstName, lastName, age} = req.body
    if(firstName && lastName && age) {
        const usersCol = req.db.collection('users')
        usersCol.insertOne({
            firstName,
            lastName,
            age
        })
        res.status(200).send()
    } else {
        res.status(400).send()
    }
})

router.get('/users', async (req, res) => {
    const usersCol = req.db.collection('users')
    const users = await usersCol.find().limit(30).toArray()
    res.json(users)
})

module.exports = router