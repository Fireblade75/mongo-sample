function refresh() {
    $.get('/api/users/', res => {
        const table = $('#tableBody')
        table.empty()
        res.map(line => {
            const tr = $('<tr/>')
            tr.append('<td>' + line.firstName + '</td>')
            tr.append('<td>' + line.lastName + '</td>')
            tr.append('<td>' + line.age + '</td>')
            table.append(tr)
        })
    })
}

$(document).ready(() => {
    $('#addButton').click((event) => {
        const user = {
            firstName: $('#firstNameField').val(),
            lastName: $('#lastNameField').val(),
            age: $('#ageField').val()
        }

        $.post('/api/users/', user, (res => {
            $('#firstNameField').val('')
            $('#lastNameField').val('')
            $('#ageField').val('')

            refresh()
        }))
    })

    $('#refreshButton').click(refresh)
    refresh()
})